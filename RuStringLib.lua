-- LibRuString v.1.2;
-- Author: Raidez 

-- If another version/copy of LibRuString is already loaded, don't load this one.
if( LibRuString ) then
	return end

LibRuString = {}

LibRuString.Settings = {}

origtostring = tostring
origtowstring = towstring
origWStringToString = WStringToString
origStringToWString = StringToWString

---------------------------------------
-- Local variables
---------------------------------------
local LibRuString = LibRuString
local pairs = pairs
local type = type
local string_byte = string.byte
local wstring_byte = wstring.byte
local string_char = string.char
local wstring_char = wstring.char
local string_len = string.len
local wstring_len = wstring.len



local firstLoad = true
local hooked = false

local old_tostring = tostring
local old_towstring = towstring
local old_WStringToString = WStringToString
local old_StringToWString = StringToWString


local function iswruchar( wchar )
	return ( (wchar >= 0x0410 and wchar <= 0x044F) or wchar == 0x0451 or wchar == 0x0401 )
end

local function isruchar( char )
	return ( (char >= 0xC0 and char <= 0xFF) or char == 0xB8 or char == 0xA8 )
end

local function towruchar( char )
	if( char >= 0xC0 and char <= 0xFF ) then
		return ( 0x0400 + ( char - 0xB0 ) )
	elseif( char == 0xB8 ) then 
		return 0x0451
	elseif( char == 0xA8 ) then 
		return 0x0401
	else
		return char
	end
end

local function toruchar( wchar )
	if( wchar >= 0x0410 and wchar <= 0x044F ) then
		return ( wchar - 0x0350 )
	elseif( wchar == 0x0401 ) then 
		return 0xA8
	elseif( wchar == 0x0451 ) then 
		return 0xB8
	else
		while wchar >= 0x0100 do
			wchar = wchar - 0x0100
		end
		return wchar
	end
end
---------------------------------------
-- End local variables
---------------------------------------


------------------------------------------------------------------------------------------------------
-- towstring analog with russian letters compatible

local function towrustring ( str, ... )
	--DEBUG(L"towstring")
	if( type( str ) == "string" ) then
		local wruStr = L""
		
		for index = 1, string_len(str) do
			wruStr = wruStr .. wstring_char( towruchar( string_byte(str, index) ) )
		end
		
		if( hooked == true ) then
			return old_towstring( wruStr, ... )
		else
			return wruStr
		end		
	else
		return old_towstring( str, ... )
	end
end

------------------------------------------------------------------------------------------------------
-- tostring analog with russian letters compatible

local function torustring ( wstr, ... )
	--DEBUG(L"tostring")
	if( type( wstr ) == "wstring" ) then
		local ruStr = ""
		
		for index = 1, wstring_len(wstr) do
			ruStr = ruStr .. string_char( toruchar( wstring_byte(wstr, index) ) )
		end
		
		if( hooked == true ) then
			return old_tostring( ruStr, ... )
		else
			return ruStr
		end
	else
		return old_tostring( wstr, ... )
	end
end

------------------------------------------------------------------------------------------------------
-- StringToWString analog with russian letters compatible

local function RuStringToWString ( str, ... )
	--DEBUG(L"StringToWString")
	if( type( str ) == "string" ) then
		local wruStr = L""
		
		for index = 1, string_len(str) do
			wruStr = wruStr .. wstring_char( towruchar( string_byte(str, index) ) )
		end
		
		if( hooked == true ) then
			results = { old_StringToWString( str, ... ) }
			results[1] = wruStr
			
			return unpack(results)
		else
			return wruStr
		end
	else
		return old_StringToWString( str, ... )
	end
end

------------------------------------------------------------------------------------------------------
-- WStringToString analog with russian letters compatible

local function RuWStringToString ( wstr, ... )
	--DEBUG(L"WStringToString")
	if( type( wstr ) == "wstring" ) then
		local ruStr = ""
		
		for index = 1, wstring_len(wstr) do
			ruStr = ruStr .. string_char( toruchar( wstring_byte(wstr, index) ) )
		end

		if( hooked == true ) then
			results = { old_WStringToString( wstr, ... ) }
			results[1] = ruStr
			
			return unpack(results)
		else
			return ruStr
		end
	else
		return old_WStringToString( wstr, ... )
	end
end

------------------------------------------------------------------------------------------------------
-- Is string with russian letters?

local function IsRuString (str)
	if( type(str) ~= "string" ) then
      return false
    end
    
	local ru = false
	
	for index = 1, string_len(str) do
		local char = string_byte(str, index)
		if( isruchar( char ) ) then
			ru = true
			break
		end
	end
	return ru
end

------------------------------------------------------------------------------------------------------
-- Is wstring with russian letters?

local function IsWRuString (wstr)
	if( type(wstr) ~= "wstring" ) then
		return false
    end
    
	local wru = false
	
	for index = 1, wstring_len(wstr) do
		local wchar = wstring_byte(wstr, index)
		if( iswruchar( wchar ) ) then
			wru = true
			break
		end
	end
	return wru
end

------------------------------------------------------------------------------------------------------
local function isru ( arg )
	if( IsRuString (arg) or IsWRuString (arg) ) then
		return true
    elseif( type(arg) == "table" ) then
		
		for index, data in pairs( arg ) do
			if( isru( index ) or isru( data ) ) then
				return true
			end
		end
		
		return false
    else
		return false
	end
end

------------------------------------------------------------------------------------------------------
function LibRuString.ToggleHook( flag )		
	if( type(flag) == "boolean" ) then
		LibRuString.Settings.forcedRuStrings = flag
	end
	
	if( ( LibRuString.Settings.forcedRuStrings == true ) and ( hooked == false ) ) then
		hooked = true
		
		old_tostring = tostring
		old_towstring = towstring
		old_WStringToString = WStringToString
		old_StringToWString = StringToWString

		tostring = torustring
		towstring = towrustring
		WStringToString = RuWStringToString
		StringToWString = RuStringToWString
		
	elseif( ( LibRuString.Settings.forcedRuStrings == false ) and ( hooked == true ) ) then
		local hookedAgain = not ( tostring == torustring and towstring == towrustring and WStringToString == RuWStringToString and StringToWString == RuStringToWString )
		
		if( hookedAgain == false ) then
		
			hooked = false
			tostring = old_tostring
			towstring = old_towstring
			WStringToString = old_WStringToString
			StringToWString = old_StringToWString
			
		else
		
			if( DialogManager ~= nil ) then
				DialogManager.MakeTwoButtonDialog( GetStringFromTable( "CustomizeUiStrings", StringTables.CustomizeUi.TEXT_UI_MOD_SETTINGS_CHANGED_DIALOG ),
					GetString( StringTables.Default.LABEL_YES ),
					function()
						BroadcastEvent( SystemData.Events.RELOAD_INTERFACE )
					end,
					GetString( StringTables.Default.LABEL_NO ),  nil )
			elseif( EA_ChatWindow ~= nil ) then
				EA_ChatWindow.Print( WRu( "RuStringsLib: ����� ��������� �������� � ����, ������������� ���������. ��� ����� ������� �������� '/reloadui'." ) )
			else
				DEBUG(L"RuStringsLib: Need to reload the UI for unhook")
			end
		end
		
	end
end

------------------------------------------------------------------------------------------------------
function LibRuString.OnLoad()
    if( firstLoad ) then
		firstLoad = false
		
		if( LibSlash ~= nil ) then
			LibSlash.RegisterWSlashCmd("forcedrustrings", function(input)
				if( input == L"true" ) then
					LibRuString.ToggleHook(true)
				elseif( input == L"false" ) then
					LibRuString.ToggleHook(false)
				end
			end)
		end
    end
    -- end firstLoad
end

------------------------------------------------------------------------------------------------------
function LibRuString.Init()
	_G["towrustring"]		= towrustring
	_G["WRu"]				= towrustring
	_G["torustring"]		= torustring
	_G["Ru"]				= torustring
	
	_G["RuWStringToString"]	= RuWStringToString
	_G["RuStringToWString"]	= RuStringToWString
	
	_G["IsWRuString"]		= IsWRuString
	_G["IsRuString"]		= IsRuString
	
	_G["isru"]				= isru
	
	LibRuString.ToggleHook()
	
	RegisterEventHandler( SystemData.Events.LOADING_END,  "LibRuString.OnLoad")
	RegisterEventHandler( SystemData.Events.RELOAD_INTERFACE,  "LibRuString.OnLoad")
end